# Apple Galette

One rustic pie using an almond flour crust and a spiced apple filling.

> **NOTE**  Best served within hours of baking

## Apple filling

* [ ] 1/4 c salted butter
* [ ] 1/2 tsp vanilla extract
* [ ] 1 lb baking apples, scrubbed, sliced 1/8" thick
* [ ] 3 Tbsp dark brown sugar
* [ ] 1 egg
* [ ] 1 Tbsp granulated sugar
* [ ] 1/2 tsp ginger, ground

### Filling preparation

1. Set pan to medium heat
2. Add butter, vanilla seeds, ginger to the pan
3. Cook until butter foams and browns for 5-8m
4. Set aside mixture
5. Prepare crust & lay crust
6. Gently toss the apples and butter mixture together

## Almond flour crust

* [ ] 2 1/2 c blanched almond flour
* [ ] 3 Tbsp fresh sage, finely chopped
* [ ] 1/3 c sugar
* [ ] 1/2 tsp sea salt
* [ ] 1/4 c unsalted butter
* [ ] 1 egg
* [ ] 1/2 tsp vanilla extract

### Crust preparation

1. Cube the butter and let it thaw in room temperature
2. Preheat oven to 350F; grease pan
3. Mix the almond flour, maple syrup, and sea salt in a large bowl
4. Separately mix the butter, egg, and vanilla extract
5. Add the wet into the dry until well combined
6. Roll out crust and gently poke holes into surface to prevent bubbling

### Final assembly

1. Measure out center with 2-3" border from the edges
2. Arrange apples, overlapping and leaving 1/2"
3. Fold edges over apple center
4. Brush the crust with egg wash (1 egg + 1 tsp water)
5. Sprinkle sugar over crust (and a little salt?)
6. Bake for 40-50m
7. Cool for 10+ minutes before serving