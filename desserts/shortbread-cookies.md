# Recipe

A gluten-free shortbread :smile:

## Assmebly

* [ ] 1 c almond flour
* [ ] 3 tbsp softened butter
* [ ] 1.5 tbsp sugar
* [ ] 1/8 tsp salt
* [ ] 1/2 tsp vanilla extract

1. Preheat oven to 350&deg;F (365&deg;F, at altitude)
2. Mix in a small bowl until a cohesive dough forms:
3. Form 1" balls of dough
4. Flatten each to about 1/4" thick
5. Bake for 8-10m, or until light golden brown

> TODO Test if another cookie shape will work
