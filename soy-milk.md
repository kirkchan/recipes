### Soy Milk
Credit goes to Andrew from their [BuzzFeed video](https://www.youtube.com/watch?v=GWoP0lc-KqY)! Watch for visual reference.
 - 500g soy beans
 - Filter bag
 - 6c water
 - 3c water
 - 1/8c sugar (experimental)

### Preparation
 - 500g soy beans
 - 6c of water
1. Soak soy beans for six hours
2. Drain the water
3. Add 6c of water and blend for several minutes until finely blended

### Filtration
 - Filter bag
 - 3c water
4. Filter the liquid into a pot and keep the ground soy beans in a separate bowl
5. Add 3c of water to the grounds and filter again

### Cooking
6. Pour the liquid into another bowl
7. Gently heat the pot with the residue until there is a browning at the bottom
8. Add the liquid into the pot
9. Whisk and boil the liquid
10. Filter the liquid again
11. Heat the liquid to a boil
12. Add sugar

### Serving
Ready to drink hot or cold. Enjoy!
