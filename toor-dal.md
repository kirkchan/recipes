# Toor Dal

An Indian-spiced lentil soup

## Lentil preparation

* 1 c split pigeon pea

1. Wash the lentils
2. Add the lentils into a pot and fill with water
3. Boil the lentils for 20m (30m at altitude)

## Assembly

 * 2 tsp avocado oil
 * 1/2 tsp cumin seeds
 * 5 (optoinal) black peppercorns
 * 1/2 tsp (optional) black mustard seeds
<br>

 * 1 onion
 * 1 inch of ginger, finely chopped
 * 5 gloves of garlic, finely chopped
<br> 

 * 1 tsp ground coriander
 * 1/2 tsp tumeric
 * 1/4 cayenne pepper powder
<br> 

 * 1 tomato
 * 1 lemon slice
 * 1 tsp salt
<br>

 * 1 c arhar dal or split pigeon pea lentil
<br>

 * cilanto, finish

### Assembly Instructions

1. Heat oil over medium-high in soup pot (4 qt+)
2. Add cumin, peppercorns, and black mustard seeds
3. Cook until sizzle and pop sounds
<br>

4. Add onion, ginger, garlic
5. Cook until translucent; stir occasionally
<br>

6. Add coriander, tumeric, cayenne; mix well
7. Add 1 tbsp of water if mixture is dry
8. Add tomato, lemon slice, and salt
9. Saute for 7m
<br>

10. Add cooked lentils and sauce to InstantPot
11. Pressure cook for 12m
<br>

12. Finish with cilanto